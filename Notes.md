# Notes

- Tileset
  - How to deal with custom GFX?
    - Define a "standard" layout to make then easily replaceble?
      - Layered make support partial?
    - Make the user put together a "map16"?
      - Only include used GFX in the level file?
  - How to integrate palettes?
    - Palette per tile?
      - Build a palette list and reuse them

- Tilemap
  - How to do "collision behavior"?

- Palette
  - Keep format with 16x16 palettes?
  - How do deal with Alpha?
    - Import RGB palette and save as custom format with RGBA?
      - `Import RGB`
      - `Import RGBA`
      - `Export RGB` (with warning if alpha would be lost)
      - `Export RGBA`
