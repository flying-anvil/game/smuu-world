var colors = [];

func loadFromFile(fileName: String):
	var file = File.new();
	file.open(fileName, File.READ);

	var colorsInPalette = file.get_len() / 3;

	for i in colorsInPalette:
		colors.append(Color(
			file.get_8() / 255.0,
			file.get_8() / 255.0,
			file.get_8() / 255.0
		));

	file.close();

func getColorByIndex(index: int) -> Color:
	if (index >= colors.size()):
		return Color(0, 0, 0);

	return colors[index];
