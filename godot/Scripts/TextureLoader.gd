extends Node

const sizePerTile = 4 * 8;
export var gfxFile = "res://gfx/Swamp.gfx";
export var palFile = "res://pal/combined.pal";
export var defaultPalette : ShaderMaterial;

func _ready():
	var Palette = preload("res://Scripts/Palette.gd")
	var palette = Palette.new()
	palette.loadFromFile(palFile)

	print(palette.getColorByIndex(134).to_html(false))
	print(palette.getColorByIndex(2543).to_html(false))

	var file = File.new()
	file.open(gfxFile, File.READ)
	var tilesInFile : int = file.get_len() / sizePerTile;

	var image = Image.new()
	image.create(8, 8, false, Image.FORMAT_R8);
	image.lock();

	for i in tilesInFile:
		var tilePos : int = i * sizePerTile;
		file.seek(tilePos)

		# Read 8 rows (tiles are 8x8)
		for y in 8:
			# Read two planar bitplanes
			var bitplane1 = file.get_8();
			var bitplane2 = file.get_8();

			# Skip to intertwined offset (16 bytes per two planar bitplanes - 2 already read)
			file.seek(file.get_position() + 14);

			var bitplane3 = file.get_8();
			var bitplane4 = file.get_8();

			# Rewind to the beginning of the next two planar bitplanes
			file.seek(file.get_position() - 16);

			for x in 8:
				var reversed =  7 - x;
				var mask = 1 << reversed;

				var bit1 = (bitplane1 & mask) >> reversed;
				var bit2 = (bitplane2 & mask) >> reversed;
				var bit3 = (bitplane3 & mask) >> reversed;
				var bit4 = (bitplane4 & mask) >> reversed;
				
				var paletteIndex = bit1 | (bit2 << 1) | (bit3 << 2) | (bit4 << 3);

				image.set_pixel(x, y, Color(
					paletteIndex / 255.0,
					0,
					0
				));

		break;

	file.close();

	image.unlock();
	var texture := ImageTexture.new();
	texture.create_from_image(image);
	texture.flags = 0;

	var sprite := $Sprite;
	sprite.texture = texture;
	applyPalette(sprite, palette, 1)

func applyPalette(sprite: Sprite, palette, paletteOffset: int = 0):
	var offset := paletteOffset * 16;
	
	var material : ShaderMaterial = defaultPalette.duplicate();
	material.resource_name = 'palette_custom';
	var color0 : Color = palette.getColorByIndex(offset);
	material.set_shader_param('C0', Color(color0.r, color0.g, color0.b, 0.0));
	
	for i in range(1, 16, 1):
		material.set_shader_param('C' + str(i), palette.getColorByIndex(i + offset))

	sprite.material = material;
